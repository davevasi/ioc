﻿<%@ Control Language="C#"
Inherits="System.Web.Mvc.ViewUserControl<IEnumerable<IOCTestMVC.Models.Ball>>" %>
<table cellspacing="0px" cellpadding="4px">
    <tr>
        <th>Ball Class</th><th>Name</th><th>Serial Number</th><th>BallType</th>
    </tr>
<% foreach (var item in Model) { %>
    <tr>
        <td><%: item.ballClass %></td>
        <td><%: item.ballname %></td>
        <td><%: item.serialnumber %></td>
        <td><%: item.balltype %></td>
      
    </tr>
<% } %>
</table>
