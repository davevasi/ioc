﻿<%@ Page Language="C#" Inherits="System.Web.Mvc.ViewPage<dynamic>" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
    "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head id="Head1" runat="server">
    <title>Bowling Ball Createor</title>
    <link rel="stylesheet"
        href="<%: Url.Content("~/Content/Styles/Site.css") %>"
        type="text/css" />
 
    <script src='<%: Url.Content("~/Content/Scripts/jquery-1.4.4.min.js") %>'
        type="text/javascript">
    </script>
</head>

<body>
       <div id="MainContainer">
        <div id="InputTrigger">
            <span>Please select the number of the Bowling Balls to retrieve</span>
            <span>
                <select id="selNoOfBowlingBalls">
                    <option>***</option>
                    <option>100</option>
                    <option>500</option>
                    <option>750</option>
                    <option>999</option>
                </select>
            </span>
            <img id="imgGetBowlingBalls"
                src="<%: Url.Content("~/Content/Images/arrow.png") %>"
                    alt="" />
        </div>
           <br />
           <br />
        <div id="HTMLContent"></div>
    </div>
</body>
</html>

<script type="text/javascript">
    var aquireBowlingBallURL = '<%: Url.Action("LoadBowlingBalls", "Home") %>';
    var ajaxLoadImgURL = '<%: Url.Content("~/Content/Images/ajax-load.gif") %>';
    $(document).ready(function () {
        var $MainContent = $("#HTMLContent");
        var $selNumberOfBowlingBalls = $("#selNoOfBowlingBalls");
        var ajaxLoadImg = "<img src='" + ajaxLoadImgURL + "' alt='Loading Balls...'/>";

        $("#imgGetBowlingBalls").click(function (e) {
            e.preventDefault();
            var numberOfBowlingBalls = $selNumberOfBowlingBalls.val();
            if (numberOfBowlingBalls == "***") {
                alert("Please select the number of the Bowling Balls to retrieve");
                return;
            }

            var resourceURL = aquireBowlingBallURL
                + "?NoOfBowlingBalls=" + numberOfBowlingBalls;
            $MainContent.html(ajaxLoadImg);
            $.ajax({
                cache: false,
                type: "GET",
                async: false,
                url: resourceURL,
                dataType: "text/html",
                success: function (htmlFragment) {
                    $MainContent.html(htmlFragment);
                },
                error: function (xhr) {
                    alert(xhr.responseText);
                }
            });
        });

        $selNumberOfBowlingBalls.change(function () {
            $MainContent.html("");
        });
    });
</script>
