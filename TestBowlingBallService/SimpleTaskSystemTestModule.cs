﻿using Abp.Modules;


namespace TestBowlingBallService
{
    [DependsOn(
        typeof(SimpleTaskSystemDataModule),
        typeof(SimpleTaskSystemApplicationModule)
    )]
    public class SimpleTaskSystemTestModule :AbpModule
    {


    }
}
