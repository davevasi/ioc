﻿using System;
using System.Collections.Generic;
using IOCTestMVC.Models;

namespace IOCTestMVC.Modules.Interface
{
    public interface IBowlingBallGenerator
    {
        List<Ball> GenerateBowlingBalls(int NoOfBowlingBalls);
    }
}
