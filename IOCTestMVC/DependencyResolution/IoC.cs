// --------------------------------------------------------------------------------------------------------------------
// <copyright file="IoC.cs" company="Web Advanced">
// Copyright 2012 Web Advanced (www.webadvanced.com)
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
// http://www.apache.org/licenses/LICENSE-2.0

// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

using StructureMap;

namespace IOCTestMVC.DependencyResolution {
	using StructureMap;
	
	public static class IoC {
		public static IContainer Initialize() {


            //this is were I would look into the xml file to see which concrete class I would be calling but ObjectFactory does not work with the new MVC5 so I need to 
            //figure out how to fix this to make it work
            
            //ObjectFactory.Initialize(x =>
            //{
            //    x.AddConfigurationFromXmlFile("StructureMap.xml");
            //});

            return new Container(c => c.AddRegistry<DefaultRegistry>());
		}
	}
}