﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using IOCTestMVC.Modules.Interface;
using IOCTestMVC.Models;

namespace IOCTestMVC.Modules.Concrete
{
    public class PremierBallGenerator  : IBowlingBallGenerator
    {
        public List<Ball> GenerateBowlingBalls(int NoOfBowlingBalls)
        {
            List<Ball> balls = new List<Ball>();
            Random rd = new Random();
            for (int i = 1; i <= NoOfBowlingBalls; i++)
            {
                Ball ball = new Ball();
                ball.ballClass = "Premier";
                ball.ballname = "SnapLock" + i.ToString();
                ball.balltype = "SNP";
                ball.serialnumber = "16SNP24G4" + i.ToString().PadLeft(i.ToString().Length + 3, '0');

                balls.Add(ball);
            }

            return balls;
        }
    }
}