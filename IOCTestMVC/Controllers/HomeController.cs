﻿using System.Web.Mvc;
using IOCTestMVC.Modules.Interface;
using StructureMap;

namespace IOCTestMVC.Controllers
{
    [HandleError]
    public class HomeController : Controller
    {
        private IBowlingBallGenerator bowlingballGenerator;
        
        [DefaultConstructor]
        public HomeController(IBowlingBallGenerator bowlingballGenerator)
        {
            this.bowlingballGenerator = bowlingballGenerator;
        }

        [HttpGet]
        public ActionResult Index() { return View(); }

        [HttpGet]
        public ActionResult LoadBowlingBalls(int NoOfBowlingballs)
        {
            
            return View(bowlingballGenerator.GenerateBowlingBalls(NoOfBowlingballs));
        }
    }
}