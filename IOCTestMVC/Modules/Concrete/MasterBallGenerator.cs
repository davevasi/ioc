﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using IOCTestMVC.Modules.Interface;
using IOCTestMVC.Models;

namespace IOCTestMVC.Modules.Concrete
{
    public class MasterBallGenerator : IBowlingBallGenerator
    {
        public List<Ball> GenerateBowlingBalls(int NoOfBowlingballs)
        {
            List<Ball> balls = new List<Ball>();
            Random rd = new Random();
            for (int i = 1; i <= NoOfBowlingballs; i++)
            {
                Ball ball = new Ball();
                ball.ballClass = "Master";
                ball.ballname = "Phaze" + i.ToString();
                ball.balltype = "PHZ";
                ball.serialnumber = "16PHZ24G4" + i.ToString().PadLeft(i.ToString().Length +3,'0');
                //student.ID = i;
                //student.Name = "Name No." + i.ToString();

                //// This generates integer scores between 0 - 40
                //student.Score = Convert.ToInt16(40 * rd.NextDouble());
                //student.Activity = "Gambling - Bad";

                balls.Add(ball);
            }

            return balls;
        }
    }
}